# infopack - A NodeJS information package implementation

This package implements the [Smart built platform - Information package framework](https://gitlab.com/sbplatform/sbp-information-package-framework). You can learn more about the framework at [sbplatform.se](http://www.sbplatform.se).

The infopack is basically a small application that generates publications (word-files, websites, etc.) out of the information package. It does this by creating and running "steps" via a pipeline. The steps manipulates the information package to create the needed outcome.

## Usage

Use npm to install the package in you app.

### Install package

```
$ npm install --save infopack
```

### In your app

```javascript
// ./index.js

// require package
var infopack = require('infopack');

// Create pipeline
var pipeline = new infopack.Pipeline([
	{
		run: function(pipeline) {
			console.log('Hello from step');
		}
	}, {
		settings: { param: 'Magic value'},
		run: function(pipeline, settings) {
			console.log('Step got some param: ' + settings.param);
		}
	}
]);

// run pipeline
pipeline.run();
```

A work folder will be generated in the project root so it might be a good idea to set this folder out of version control.

```
// ./.gitignore

node_modules/
work_folder/
```

## Pipeline

The pipeline is constructed with one or more steps, a step is basically a function with its optional parameters, If async tasks are performed this function need to return a promise. The step can hand written in your app or generated from a generator.

### Step definition

**Basic step**

```
{
	run: function(pipeline) {
		return Promise.resolve({ data: { ... } });
	}
}
```

**Advanced step**

```
{
	settings: { key: 'value' },
	run: function(pipeline, settings) {
		console.log(settings.key); // -> value
	}
}
```

### Run cycle

After a pipeline is created and filled with steps you simply run it by `pipeline.run()`. A work folder will be created/reset with every run. A `log.json` file is also created in the work folder.

If an error is thrown during run the pipeline will catch it, put it in the log and stop.

It is possible to add log messages during the step by using `pipeline.addLog`.

### Store data between steps

The pipeline instance are sent to each run function as the first parameter. We can accumulate data from each step for later use.

## API

### new Pipeline([inputSteps], [options]);

| Property            | Type   | Definition                                                                         | Default     | Example |
|---------------------|--------|------------------------------------------------------------------------------------|-------------|---------|
| inputSteps          | step[] |                                                                                    | []          |         |
| options             | object | Optional options object                                                            | {}          |         |
| options.basePath    | string | Declare base path for the infopack, this path will be resolved to an absolute path | ./          |         |
| options.workDirName | string | Specify the name of the working folder                                             | work_folder |         |
| options.template    | string | Provide a custom template to the defualt static generator                          |             |         |


### pipeline.addStep(step)

Add a single step to the end of the pipeline.

### pipeline.addLog(message, [logLevel])

Adds a log message to the log history, a option log level can be set. Defaults to `info`.

### pipeline.getKey('key')

Returns the value for the specified key, if any.

### pipeline.getKeys()

Returns a copy of all keys in the pipeline.

### pipeline.setKey('key', 'value')

Sets a value fro the specified key, can be of any type.

### pipeline.getBasePath()

Returns the base path for the infopack.

### pipeline.getWorkDirPath()

Returns the work directory path (in basepath).

### pipeline.getOutputDirPath()

Returns the output directory path (in basepath).

### pipeline.writeFile(fileObj)

| Property              | Type   | Definition                                                                 | Default | Example |
|-----------------------|--------|----------------------------------------------------------------------------|---------|---------|
| fileObj               | object |                                                                            |         |         |
| fileObj.path          | string | File path (folder structure must exist)                                    |         |         |
| fileObj.data          | string | File data, can be a string or Buffer ( will be sent to fs.writeFileSync()) |         |         |
| [fileObj.title]       | string | File title                                                                 |         |         |
| [fileObj.description] | string | Optional file description                                                  |         |         |

### pipeline.prettyTable(['info', 'debug', 'error'])

Returns a "consolable" string with the log as a table. An optional array with levels can be provided for filtering purposes.

## Generators

Generators are small applications that generates a step which can in the pipeline. Generators should be as generic as possible for compability purposes.
