import { Executor, InfopackContentInput, PipelineStep } from '../../lib/pipeline'
import Pipeline from '../../index'
import { step as folderToInfopack } from '../../lib/generators/folder-to-infopack'

const run = function (executor: Executor) {
  executor.toOutput({
    path: 'zzz.snippet.html',
    title: 'zzz Snippet',
    description: 'Some fancy description',
    data: Buffer.from('<h1>En rubrik</h1><p>Hej detta är en mening</p>')
  } as InfopackContentInput)

  executor.toOutput({
    path: 'zzz/zzz.text',
    filename: 'zzz.txt',
    title: 'A som i alfabetet',
    description: 'Some fancy description',
    data: Buffer.from('some data')
  } as InfopackContentInput)

  executor.toOutput({
    path: 'test/aaa.txt',
    filename: 'aaa.txt',
    title: 'A som i alfabetet',
    description: 'Some fancy description',
    data: Buffer.from('some data')
  } as InfopackContentInput)

  executor.toOutput({
    path: 'aaa.text',
    title: 'A som i alfabetet',
    description: 'Some fancy description',
    data: Buffer.from('some data')
  } as InfopackContentInput)

  executor.toOutput({
    path: 'test-mapp/aaa.txt',
    title: 'A som i alfabetet',
    description: 'Some fancy description',
    data: Buffer.from('some data')
  } as InfopackContentInput)

  executor.toOutput({
    path: 'test-mapp/AAA.txt',
    title: 'A som i alfabetet',
    description: 'Some fancy description',
    data: Buffer.from('some data')
  } as InfopackContentInput)

  return Promise.resolve()
}

const steps: PipelineStep[] = [
  folderToInfopack(),
  new PipelineStep(run)
]

const pipeline = new Pipeline(steps, { basePath: 'tests/bad-json-test/test_dir' })

pipeline.run()
